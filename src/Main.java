public class Main
{
	public static int randomBetween(int min, int max)
	{
		return (int) ((Math.random() * (max - min)) + min);
	}
	
	public static void main(String[] args)
	{
		Gramatica gramatica = new Gramatica();
		gramatica.citire("src/ReadFile.txt");
		gramatica.afisare();
		if (!gramatica.verificare())
		{
			System.out.println("The verification was unsuccessful.");
			return;
		}
		System.out.println(gramatica.generare(false));
		System.out.println(gramatica.generare(false));
		System.out.println(gramatica.generare(false));
		System.out.println(gramatica.generare(true));
		System.out.println(gramatica.generare(true));
		System.out.println(gramatica.generare(true));
		System.out.println(gramatica.generare(true));
		System.out.println(gramatica.generare(true));
		System.out.println(gramatica.generare(true));
		System.out.println(gramatica.generare(true));
	}
}

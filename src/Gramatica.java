import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

@SuppressWarnings("SpellCheckingInspection")
public class Gramatica
{
	private String VN;
	private String VT;
	private char S;
	private ArrayList<Pair<String, String>> P;
	
	public void citire(String fileName)
	{
		try
		{
			File file = new File(fileName);
			Scanner scanner = new Scanner(file);
			
			this.VN = scanner.next();
			this.VT = scanner.next();
			this.S = scanner.next().charAt(0);
			this.P = new ArrayList<Pair<String, String>>();
			
			while (scanner.hasNext())
			{
				Pair<String, String> readPair = new Pair<String, String>(scanner.next(), scanner.next());
				P.add(readPair);
			}
			scanner.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Could not open the file: " + fileName);
		}
	}
	
	public void afisare()
	{
		System.out.println("VN = " + VN);
		System.out.println("VT = " + VT);
		System.out.println("S = " + S);
		System.out.println("Productii: ");
		for (int i = 0; i < P.size(); i++)
		{
			System.out.println(i + 1 + ". " + P.get(i));
		}
	}
	
	public boolean verificare()
	{
		if (VN.indexOf(S) < 0)
			return false;
		
		
		boolean found = false;
		for (char character: VN.toCharArray())
		{
			if (VT.indexOf(character) >= 0)
			{
				found = true;
				break;
			}
		}
		if (found)
		{
			return false;
		}
		
		
		boolean foundS = false;
		for (Pair<String, String> pair: P)
		{
			if (pair.getKey().toLowerCase().equals(pair.getKey()))
			{
				return false;
			}
			
			if (!foundS && pair.getKey().indexOf(S) >= 0)
			{
				foundS = true;
			}
			
			String VNVT = VN + VT;
			for (char character: (pair.getKey() + pair.getValue()).toCharArray())
			{
				if (character == '&')
				{
					continue;
				}
				if (VNVT.indexOf(character) < 0)
				{
					return false;
				}
			}
		}
		
		return foundS;
	}
	
	public String generare()
	{
		return generare(false);
	}
	
	public String generare(boolean showSteps)
	{
		String result = String.valueOf(S);
		ArrayList<Pair<String, String>> possibleProductions = new ArrayList<Pair<String, String>>();
		
		while (!result.toLowerCase().equals(result))
		{
			possibleProductions.clear();
			
			// Fill the list with the possible productions
			for (Pair<String, String> pair: P)
			{
				if (result.contains(pair.getKey()))
				{
					possibleProductions.add(pair);
				}
			}
			
			// Pick a random production from the list
			Pair<String, String> pickedProduction = possibleProductions.get(Main.randomBetween(0, possibleProductions.size()));
			
			// Apply the picked production
			if (showSteps)
			{
				System.out.print(result + " -> (" + pickedProduction.getKey() + " -> " + pickedProduction.getValue() + ") -> ");
			}
			
			result = result.replaceFirst(pickedProduction.getKey(), pickedProduction.getValue());
		}
		
		result = result.replace("&", "");
		return result;
	}
}